
const express = require('express');
const router = express.Router();
const validator = require('../utils/validator');
const env = require("../services/env").shared;
const errors = require("../utils/errors");

router.get('/getEnvironment/:process', validator.validate_getEnvVar, async (req, res) => {
    const { ok, data, error, display } = await env.getEnvVariables(req.params.process);
    if (!ok) {
        if (!display) {
            return internalError(res)
        }
        return res.status(display.status)
            .end(display.desc)
    }
    return res.status(200)
        .send(data)
})
router.get('/setEnvironment/:process/:key/:value', validator.validate_setEnvVar, async (req, res) => {
    const { ok, data, error, display } = await env.setEnvVariables(req.params.process, req.params.key, req.params.value);
    if (!ok) {
        if (!display) {
            return internalError(res)
        }
        return res.status(display.status)
            .end(display.desc)
    }
    return res.status(200)
        .send(data)
})
router.use('/', (req, res)=> {
    return res.status(errors.BAD_REQUEST.status)
        .end(errors.BAD_REQUEST.desc)
})

module.exports = router;