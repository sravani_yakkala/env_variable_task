"use strict";
const fs = require('fs');
const path = require('path');
const callbacks = require("../utils/callbacks").shared;
const errors = require("../utils/errors");

class Env {
    constructor() {
        this.filePath = path.join(__dirname, '..', 'program_files');
    }
    getEnvVariables(process) {
        let me = this;
        return new Promise(async function (resolve) {
            try {
                let pathToEnv = path.join(me.filePath, process , '.env');
                let fileExists = fs.existsSync(pathToEnv);
                if (fileExists) {
                    let content = fs.readFileSync(pathToEnv, 'utf8').toString().split('\n');
                    let envData = {},key_value;
                    content.forEach((line)=>{
                        key_value = line.split('=');
                        envData[key_value[0]] =key_value[1]
                    });
                    callbacks.success(resolve, envData)
                }
                else {
                    callbacks.fail(resolve, errors.NO_FILE_EXISTS.desc, errors.NO_FILE_EXISTS)
                }
            } catch (e) {
                callbacks.fail(resolve, new Error(e), errors.INTERNAL_ERROR)
            }
        })
    }
    setEnvVariables(process, key, value) {
        let me = this;
        return new Promise(async function (resolve) {
            try {
                let pathToEnv = path.join(me.filePath, process,  '.env');
                let fileExists = fs.existsSync(pathToEnv);
                let envData = {};
                if (fileExists) {
                    let content = fs.readFileSync(pathToEnv, 'utf8').toString().split('\n');
                    let key_value;
                    content.forEach((line)=>{
                        key_value = line.split('=');
                        envData[key_value[0]] =key_value[1]
                    });
                    if(!envData[key]){
                        fs.appendFileSync(pathToEnv,'\n'+ key + '=' + value);
                        envData[key] = value;
                    }
                }
                else {
                     envData[key] = value;
                    fs.mkdirSync( path.join(me.filePath, process));
                    fs.writeFileSync(pathToEnv, key + '=' + value);
                }
                callbacks.success(resolve, envData);
            } catch (e) {
                callbacks.fail(resolve, new Error(e), errors.INTERNAL_ERROR)
            }
        })
    }
}

Env.shared = new Env();
module.exports = Env;