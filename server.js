const express = require('express');
const bodyParser = require('body-parser');

const config = require("./config/config");
const utils = require("./utils/logger").shared;
const logger = utils.getLogger();
const env = require("./routes/env");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/',env);

app.listen(process.env['PORT'] || config.port, () => {
    logger.log("server is running on port", config.port);
});