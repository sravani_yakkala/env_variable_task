const errors = {
    "BAD_REQUEST": {
        status: 401,
        desc: "bad request"
    },
    "INTERNAL_ERROR": {
        status: 500,
        desc: "internal error",
        error:"error"
    },
    "INCOMPLTE": {
        status: 401,
        desc: "incomplete data"
    },
    "UNABLE": {
        status: 422,
        desc: "unable to process"
    },
    "UN_AUTHORIZED": {
        status: 401,
        desc: "authorization required"
    },
    "NO_FILE_EXISTS": {
        status: 401,
        desc: "there is no file exists with given inputs"
    }
}
module.exports = errors