const errors = require("./errors")

exports.validate_getEnvVar = function (req, res, next) {
    let data = req.params;
    if (!data || !data.process) {
        return res.status(errors.BAD_REQUEST.status)
            .end(errors.BAD_REQUEST.desc)
    }
    return next();
}
exports.validate_setEnvVar = function (req, res, next) {
    let data = req.params;
    if (!data || !data.process || !data.key || !data.value) {
        return res.status(errors.BAD_REQUEST.status)
            .end(errors.BAD_REQUEST.desc)
    }
    return next();
}
