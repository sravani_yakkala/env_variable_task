"use strict";
const tracer = require('tracer');

class Logger {
    constructor() {
        this.logger = null;
    }
    getLogger() {
        if (this.logger) {
            return this.logger;
        }
        this.logger = tracer.console({
            format: "{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})",
            dateformat: "dd-mm-yyyy HH:MM:ss TT"
        });
        return this.logger;
    }
}

Logger.shared = new Logger();

module.exports = Logger;

