"use strict";
// const utils = require("../utils").shared;
// const logger = utils.getLogger();

class Callbacks {
    constructor() {
    }

    fail(resolve, err, display) {
        console.log("Error:", err || "no error", ",   Display:", display || "");
        resolve({
            ok: false,
            error: err,
            display: display
        })
    }

    success(resolve, data) {
        resolve({
            ok: true,
            data: data
        })
    }


}

Callbacks.shared = new Callbacks();
module.exports = Callbacks;